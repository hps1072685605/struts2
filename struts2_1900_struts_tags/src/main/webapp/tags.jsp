<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: hps
  Date: 2017/8/6
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Struts-Tags学习</title>
</head>
<body>
   <ol>
       <li>property：<s:property value="username"/> </li>
       <li>property 取值为字符串：<s:property value="'username'"/> </li>
       <li>property 设定默认值：<s:property value="admin" default="管理员"/> </li>
       <li>property 设定HTML：<s:property value="'<hr>'" escape="false"/> </li>
       <hr>
       <li>set 设定adminName值（默认为request和ActionContext）<s:set var="adminName" value="username"/> </li>
       <li>set 从request取值：<s:property value="#request.adminName"/> </li>
       <li>set 从ActionContext取值：<s:property value="#adminName"/> </li>
       <li>set 设定范围：<s:set var="adminPassword" value="password" scope="session"/> </li>
       <li>set 从相应范围取值：<%=session.getAttribute("adminPassword")%></li>
       <li>set 设定var，范围为ActionContext：<s:set value="password" var="a" scope="session"/> </li>
       <li>set 使用#取值：<s:property value="#session.a"/> </li>
       <hr>
       <li>
           bean 定义bean，并使用param来设定新的属性值：
           <s:bean name="com.test.struts2_1900.model.Dog">
               <s:param value="'dogou'" name="name"></s:param>
               <s:property value="name"/>
           </s:bean>
       </li>
       <li>
           bean 查看debug情况：
           <s:bean name="com.test.struts2_1900.model.Dog" var="myDog">
               <s:param name="name" value="'oudy'"></s:param>
           </s:bean>
           <s:property value="#myDog.name"/>
           <s:debug></s:debug>
       </li>
       <hr>

       <li>if elseif else:
           age = <s:property value="#parameters.age[0]"/><br>
           <s:if test="#parameters.age[0] < 0">wrong age!</s:if>
           <s:elseif test="#parameters.age[0] < 20">too young!</s:elseif>
           <s:else>yeah!</s:else><br>

           <s:if test="#parameters.aaa == null">null</s:if>
       </li>

       <li>遍历集合：<br>
           <s:iterator value="{1, 2, 3}">
               <s:property/>
           </s:iterator>
       </li>
       <li>自定义变量：<br>
           <s:iterator value="{'aaa', 'bbb', 'ccc'}" var="x" >
               <s:property value="#x.toUpperCase()"/>
           </s:iterator>
       </li>
       <li>使用status：<br>
           <s:iterator value="{'aaa', 'bbb', 'ccc'}" status="status" >
               遍历过的元素总数：<s:property value="#status.count"/> |
               遍历过的元素索引：<s:property value="#status.index"/> |
               当前是偶数？：<s:property value="#status.even"/> |
               当前是奇数？：<s:property value="#status.odd"/> |
               是第一个元素吗？：<s:property value="#status.first"/> |
               是最后一个元素吗？：<s:property value="#status.last"/>
               <br>
           </s:iterator>
       </li>
       <li>
           <s:iterator value="#{1:'a', 2: 'b', 3:'c'}">
               <s:property value="key"/> | <s:property value="value"/><br>
           </s:iterator>
       </li>

       <li>
           <s:iterator value="#{1:'a', 2: 'b', 3:'c'}" var="x">
               <s:property value="#x.key"/> | <s:property value="#x.value"/><br>
           </s:iterator>
       </li>
       <li>
           <s:fielderror fieldName="fielderror.test" theme="simple"></s:fielderror>
       </li>
   </ol>
</body>
</html>
