package com.test.struts2_0900.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.test.struts2_0900.model.User;

public class UserAction extends ActionSupport implements ModelDriven<User>{
    private User user = new User();

    public String add() {
        System.out.println("name=" + user.getName());
        System.out.println("age=" + user.getAge());
        return SUCCESS;
    }

    @Override
    public User getModel() {
        return user;
    }
}
