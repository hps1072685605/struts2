<%--
  Created by IntelliJ IDEA.
  User: hps
  Date: 2017/8/4
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>$Title$</title>
  </head>
  <body>
     <p>Action执行的时候不一定要执行execute方法</p>
     <p>可以在配置文件中配置Action的时候用method=来指定执行哪个方法，也可以在url地址中动态指定（动态方法调用DMI）（推荐）</p>
     <a href="user/userAdd">添加用户</a><br>
     <a href="user/user!add">添加用户</a><br>
     <p>前者会产生太多的action，所以不推荐使用</p>
  </body>
</html>
0