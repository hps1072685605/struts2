<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: hps
  Date: 2017/8/5
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
  <h2>User Add Error!</h2>
  <s:fielderror fieldName="name" theme="simple"/>
  <br>
  <s:property value="errors.name[0]"/>
  <s:debug></s:debug>
</body>
</html>
