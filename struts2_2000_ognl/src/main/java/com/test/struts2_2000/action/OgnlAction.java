package com.test.struts2_2000.action;

import com.opensymphony.xwork2.ActionSupport;
import com.test.struts2_2000.model.Cat;
import com.test.struts2_2000.model.Dog;
import com.test.struts2_2000.model.User;

import java.util.*;

public class OgnlAction extends ActionSupport {
    private String username;
    private String password;
    private User user;
    private Cat cat;
    private List<User> users = new ArrayList<>();
    private Set<Dog> dogs = new HashSet<>();
    private Map<String, Dog> dogMap = new HashMap<>();

    public OgnlAction() {
        users.add(new User(1));
        users.add(new User(2));
        users.add(new User(3));

        dogs.add(new Dog("dog1"));
        dogs.add(new Dog("dog2"));
        dogs.add(new Dog("dog3"));

        dogMap.put("dog100", new Dog("100"));
        dogMap.put("dog101", new Dog("101"));
        dogMap.put("dog102", new Dog("102"));
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }

    public String m() {
        return "hello";
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Set<Dog> getDogs() {
        return dogs;
    }

    public void setDogs(Set<Dog> dogs) {
        this.dogs = dogs;
    }

    public Map<String, Dog> getDogMap() {
        return dogMap;
    }

    public void setDogMap(Map<String, Dog> dogMap) {
        this.dogMap = dogMap;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }
}
