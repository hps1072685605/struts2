package com.test.struts2_2000.model;

public class User {
    private int age;

    public User() {
        System.out.println("user constructor");
    }

    public User(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "age=" + age +
                '}';
    }
}
