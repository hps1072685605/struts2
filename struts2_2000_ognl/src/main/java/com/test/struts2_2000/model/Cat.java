package com.test.struts2_2000.model;

public class Cat {
    private Dog friend;

    public Dog getFriend() {
        return friend;
    }

    public void setFriend(Dog friend) {
        this.friend = friend;
    }

    public String miaomiao() {
        return "miaomiao";
    }
}
