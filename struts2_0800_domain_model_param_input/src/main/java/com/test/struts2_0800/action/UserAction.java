package com.test.struts2_0800.action;

import com.opensymphony.xwork2.ActionSupport;
import com.test.struts2_0800.model.User;

public class UserAction extends ActionSupport{
    private User user;

    public String add() {
        System.out.println("name=" + user.getName());
        System.out.println("age=" + user.getAge());
        return SUCCESS;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
