<%--
  Created by IntelliJ IDEA.
  User: hps
  Date: 2017/8/4
  Time: 21:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>$Title$</title>
  </head>
  <body>
    <p>使用通配符，将配置量降到最低</p>
    <a href="actions/Studentadd">添加学生</a>&nbsp;
    <a href="actions/Studentdelete">删除学生</a><br>
    <p>不过，一定要遵守“约定优于配置”的原则</p>
    <a href="actions/Teacher_add">添加老师</a>&nbsp;
    <a href="actions/Teacher_delete">删除老师</a>&nbsp;
    <a href="actions/Course_add">添加课程</a>&nbsp;
    <a href="actions/Course_delete">删除课程</a>
  </body>
</html>
