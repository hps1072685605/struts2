<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/8/4
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>struts2路径</title>
</head>
<body>
  <h2>struts2路径</h2>
  <div>
      <p>struts2中的路径问题是根据action的路径而不是根据jsp路径来确定，所以尽量不要使用相对路径</p>
      <div>
          <a href="index.jsp">index,jsp</a>
      </div>
      <p>虽然可以用redirect方式解决，但redirect方式并非必要</p>
      <p>解决方法非常简单，统一使用绝对路径。（在jsp中用request.getContextRoot()方式来拿到webapp的路径）</p>
      <p>或者指定basePath</p>
  </div>
</body>
</html>
