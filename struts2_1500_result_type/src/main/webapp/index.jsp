<%--
  Created by IntelliJ IDEA.
  User: hps
  Date: 2017/8/5
  Time: 17:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
  <h3>Result类型</h3>
  <ol>
      <li><a href="r/r1">dispatcher</a></li>
      <li><a href="r/r2">redirect</a></li>
      <li><a href="r/r3">chain</a></li>
      <li><a href="r/r4">redirectAction</a></li>
      <li>freemarker</li>
      <li>httpheader</li>
      <li>stream</li>
      <li>velocity</li>
      <li>xslt</li>
      <li>plaintext</li>
      <li>tiles</li>
  </ol>
</body>
</html>
